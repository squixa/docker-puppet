FROM ubuntu:14.04

ARG puppet_version=3.8
ARG puppetlabs_stdlib_version=4.15.0
ARG puppetlabs_apt_version=1.8.0

# http://apt.puppetlabs.com/puppetlabs-release-trusty.deb
COPY puppetlabs-release-trusty.deb /

RUN sed '/^deb-src/d;' /etc/apt/sources.list -i && \
  dpkg --install /puppetlabs-release-trusty.deb && \
  apt-get update && \
  apt-get install --assume-yes --no-install-recommends \
    git \
    "puppet=${puppet_version}*" \
    "puppet-common=${puppet_version}*" && \
  sed '/^templatedir=/d' -i /etc/puppet/puppet.conf

RUN puppet module install puppetlabs-stdlib --version "${puppetlabs_stdlib_version}"
RUN puppet module install puppetlabs-apt --version "${puppetlabs_apt_version}"
